using System;
using System.IO;
using System.Runtime.InteropServices;

class XcpTest
{
    [DllImport("libxcp.so")] static extern IntPtr xcp_init(string path);
    [DllImport("libxcp.so")] static extern void xcp_free(IntPtr ctx);
    [DllImport("libxcp.so")] static extern int svod_decompress(IntPtr ctx);

    public static void Main(string[] args)
    {
        if (args.Length == 0)
            return;

        if (!File.Exists(args[0]))
        {
            Console.WriteLine("File does not exist: " + args[0]);
            return;
        }

        IntPtr xcp_ctx = xcp_init(args[0]);
        svod_decompress(xcp_ctx);
        xcp_free(xcp_ctx);
    }
}