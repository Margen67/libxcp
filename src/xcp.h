#ifndef XCP_H
#define XCP_H

#include "inflate.h"
#include "util.h"

#define XCP_MAGIC 0x789C

enum XCP_TYPE
{
  XCP_STFS,
  XCP_SVOD,
  XCP_INVALID
};

// XCP Context
typedef struct _XCP_CTX
{
  // XCP Type (SVOD or STFS)
  enum XCP_TYPE type;

  // File Pointer
  FILE *f;
  // Path to XCP file
  char path[256];
  // Path to Output
  char path_out[256];
  // Extracted Header Path
  char path_header[256];
  // Extracted Data Directory
  char path_data[256];

  // Decompressed Data Buffer (1MB)
  void *data;
  // Size of Data Buffer
  size_t data_size;
  // Compressed Size
  size_t size_c;
  // Decompressed Size
  size_t size_d;
  // Zlib Stream Handle
  z_stream strm;

  // Title
  wchar_t title[64];
  // Title ID
  uint32_t title_id;
  // Content ID / SHA-1
  uint8_t content_id[20];
  // Content ID / SHA-1 Hex String
  char content_id_str[41];

} XCP_CTX;

// Initialize XCP Context
EXPORT XCP_CTX *xcp_init(char *path);
EXPORT void xcp_free(XCP_CTX *ctx);
EXPORT int xcp_read_header(XCP_CTX *ctx);
EXPORT void xcp_print_header(XCP_CTX *ctx);

void xcp_mkdir(XCP_CTX *ctx);

#endif